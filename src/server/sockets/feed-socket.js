const WebSocket = require('ws');

const app = (app) => {
  const ews = require('express-ws')(app);
  const clients = ews.getWss().clients;

  app.ws('/feed', function (ws, req) {

      ws.on('message', fromClient => {
        const dto = JSON.parse(fromClient);
        // What happens when a message comse in.  Typically, this is where
        // You tell other clients connected about what the fuck is going on.
          distributeSomething(/* whatever you wanna tell the clients */);
      })

      ws.on('close', () => {
        //Connection closed.  Something specific you wanna do?
      });
  });

  const distributeSomething = (msg) => {
    clients.forEach(client => {
      if (client.readyState === WebSocket.OPEN) {
        client.send(JSON.stringify({
          posts: msg,
          noClient: clients.size
        }));
      }
    })
  };
};

module.exports = app;
