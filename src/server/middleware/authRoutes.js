const express = require('express');
const router = express.Router();
const passport = require('passport');
const userRepo = require('../db/userRepo.js');

  router.post('/login', passport.authenticate('local'), (req, res) => {
    res.status(204).send();
  });

  router.post('/register', (req, res) => {
    const {username, password, birthday, country} = req.body;

    const created = userRepo.createUser(username, password, birthday, country);

    if(!created) {
      res.status(400).send();
      return;
    }

    passport.authenticate('local')(req, res, () => {
      req.session.save(err => {
        if(err) {
          res.status(500).send()
        } else {
          res.status(201).send()
        }
      })
    })
  });

  //TODO(Håvard) Implement as a post. Change the test if you change this
  router.get('/logout', function(req, res){
    req.logout();
    res.status(204).send();
  });

  router.get("/user", (req, res) => {
    if(req.user) {
      res.status(200).json({
        userId: req.user.id,
      });
      return
    }
    res.status(401).send()
  });

  router.get("/users", (req, res) => {
    const users = userRepo.getAllUsers();
    res.status(200).json({users})
  });

module.exports = router;
