/*
   This file is influenced, but not explictly copied from the course content.
   https://github.com/arcuri82/web_development_and_api_design/blob/master/les07/server_client_together/src/server/app.js
 */

const postRepo = require('../db/postRepo.js');

module.exports = app => {
  app.get("/api/posts/:id", (req, res) => {
    // this is an example route for one specific  post
  });

  app.get("/api/posts", (req, res) => {
    // get all posts
  });

  app.post("/api/posts", (req, res) => {
    // Create a post
  })
};
